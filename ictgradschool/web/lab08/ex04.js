"use strict";

// FUNCTIONS
// ------------------------------------------

// TODO Complete this function, which should generate and return a random number between a given lower and upper bound (inclusive)

function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};
var h1 = getRndInteger(25, 50);
var r1 = getRndInteger(25, 50);
var h2 = getRndInteger(25, 50);
var r2 = getRndInteger(25, 50);
console.log(h1, r1, h2, r2);

// TODO Complete this function, which should round the given number to 2dp and return the result.

var number10 = Math.random() + Math.floor(Math.random() * 100);
function roundTo2dp(number10) {
    return Math.floor(100 * number10) / 100
};

var result2 = roundTo2dp(number10);
console.log(result2);

// TODO Write a function which calculates and returns the volume of a cone.

function coneV(r1, h1) { return Math.floor(100 * Math.PI * Math.pow(r1, 2) * h1 / 3) / 100; };
var V1 = coneV(r1, h1);
console.log("The cone's volume is " + V1 + "m3");


// TODO Write a function which calculates and returns the volume of a cylinder.

function cylinderV(r2, h2) { return Math.floor(100 * Math.PI * Math.pow(r2, 2) * h2) / 100; };

var V2 = cylinderV(r2, h2);
console.log("The cylinder's volume is " + V2 + "m3");

var myArray = [V1, V2];
var shape ;
if (V1>V2) {
    shape= "cone";
}
    else if (V1<V2) {
shape="cylinder";
  
}

var sorted = myArray.sort(function (a, b) { return a - b });
console.log(sorted);
console.log("The shape with the largest volume is " + shape + ", with a volume of " + myArray[1]+ " cm^3");

// TODO Write a function which prints the name and volume of a shape, to 2dp.
// function volumeCalcs(operation, r, h) {
//     var vResult = operation(r, h);
//     console.log("The volume of the " + volumeCalcs.toString + Math.floor (100*vResult)/100 + "cm ^3");
// };

// var vResult1= volumeCalcs (cylinderV, 3,5 );

// Console.log ( "The volume of the "  + Math.floor (vResult1* 100)/100 + "cm^3");

// ------------------------------------------

// TODO Complete the program as detailed in the handout. You must use all the functions you've written appropriately.
