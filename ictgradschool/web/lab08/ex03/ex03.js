"use strict";

// FUNCTIONS
// ------------------------------------------

// TODO Complete this function, which should generate and return a random number between a given lower and upper bound (inclusive)

function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};
var result = getRndInteger(1, 30);
console.log(result);

// TODO Complete this function, which should round the given number to 2dp and return the result.

var number10 = Math.random() + Math.floor(Math.random() * 100);
function roundTo2dp(number10) {
    return Math.floor(100 * number10) / 100
};

var result2 = roundTo2dp(number10);
console.log(result2);

// TODO Write a function which calculates and returns the volume of a cone.

function coneV(r, h) { return Math.PI * Math.pow(r, 2) * h / 3; };
var V1 = coneV(2, 3);
console.log("The cone's volume is " + Math.floor(100 * V1) / 100 + "m3");


// TODO Write a function which calculates and returns the volume of a cylinder.

function cylinderV(r, h) { return Math.PI * Math.pow(r, 2) * h };

var V2 = cylinderV(2, 3);
console.log("The cylinder's volume is " + Math.floor(100 * V2) / 100 + "m^3");

// TODO Write a function which prints the name and volume of a shape, to 2dp.
function volumeCalcs(operation, r, h) {
    var vResult = operation(r, h);
    console.log("The volume of the " + 
     + Math.floor(100*vResult) /100 + "cm ^3");
};

var vResult1= volumeCalcs(cylinderV, 3,5);

// console.log(vResult1);
// console.log( "The volume of the " + Math.floor(vResult1* 100)/100 + "cm^3");

// ------------------------------------------

// TODO Complete the program as detailed in the handout. You must use all the functions you've written appropriately.
